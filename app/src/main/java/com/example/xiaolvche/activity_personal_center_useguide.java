package com.example.xiaolvche;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class activity_personal_center_useguide extends AppCompatActivity {

    //按钮声明
    private Button useguide_return;
    private Button app_useguide;
    private Button car_useguide;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_useguide);

        //返回界面跳转
        useguide_return = findViewById(R.id.mc_return);
        useguide_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.d("==========", "onClick: click");
                Intent intent=new Intent(activity_personal_center_useguide.this,activity_personal_center.class);
                startActivity(intent);
            }
        });

        //app_useguide界面跳转
        app_useguide = findViewById(R.id.mc_app_use_guide_value);
        app_useguide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.d("==========", "onClick: click");
                Intent intent=new Intent(activity_personal_center_useguide.this,activity_personal_center_useguide_app.class);
                startActivity(intent);
            }
        });

        //car_useguide界面跳转
        car_useguide = findViewById(R.id.mc_instructions_for_vehicle_use_value);
        car_useguide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.d("==========", "onClick: click");
                Intent intent=new Intent(activity_personal_center_useguide.this,activity_personal_center_useguide_car.class);
                startActivity(intent);
            }
        });

    }
}
