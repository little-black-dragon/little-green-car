package com.example.xiaolvche;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class Setup_change_passwordActivity extends AppCompatActivity {

    private Button change_password_return;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_up_change_password);

        change_password_return = findViewById(R.id.mc_return);
        change_password_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.d("==========", "onClick: click");
                Intent intent=new Intent(Setup_change_passwordActivity.this,SetupActivity.class);
                startActivity(intent);
            }
        });
    }
}
