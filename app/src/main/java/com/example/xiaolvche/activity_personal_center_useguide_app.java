package com.example.xiaolvche;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class activity_personal_center_useguide_app extends AppCompatActivity {

    //按钮声明
    private Button app_useguide_return;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_useguide);

        //返回界面跳转
        app_useguide_return = findViewById(R.id.mc_return);
        app_useguide_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.d("==========", "onClick: click");
                Intent intent=new Intent(activity_personal_center_useguide_app.this,activity_personal_center_useguide.class);
                startActivity(intent);
            }
        });


    }
}
