package com.example.xiaolvche;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class Setup_about_usActivity extends AppCompatActivity {

    private Button about_us_return;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_up_about_us);

        about_us_return = findViewById(R.id.mc_return);
        about_us_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.d("==========", "onClick: click");
                Intent intent=new Intent(Setup_about_usActivity.this,SetupActivity.class);
                startActivity(intent);
            }
        });
    }
}
