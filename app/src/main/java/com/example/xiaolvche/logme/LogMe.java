package com.example.xiaolvche.logme;

import android.util.Log;

import androidx.annotation.Nullable;

import com.example.xiaolvche.MyApp;

public class LogMe {

    public interface LogRunnable{
        void log(String tag, String msg);
    }

    public static volatile StringBuffer log = new StringBuffer();

    private static volatile LogRunnable e, w, i;
    private static final LogRunnable no = (tag, msg) -> {};

    private static volatile boolean initialized = false;

    public static void init(){
        if (!initialized) {
            synchronized (LogMe.class) {
                if (!initialized) {
                    defaultSet();
                    initialized = true;
                }
            }
        }
    }

    public static synchronized void setE(LogRunnable e){
        LogMe.e = e;
    }
    public static synchronized void setW(LogRunnable w){
        LogMe.w = w;
    }
    public static synchronized void setI(LogRunnable i){
        LogMe.i = i;
    }
    public static synchronized void setAll(LogRunnable logRunnable){
        LogMe.e = LogMe.w = LogMe.i = logRunnable;
    }
    public static synchronized void defaultSet(){
        if (MyApp.isDebug()) {
            setE(Log::e);
            setW(Log::w);
            setI(Log::i);
        } else {
            setAll(no);
        }
    }

    private static void write_log(String tag, String msg){
        if (log.length() >= 33554432){
            synchronized (LogMe.class) {
                if (log.length() >= 33554432) {
                    log = new StringBuffer();
                }
            }
        }
        log.append(tag).append(": ").append(msg).append("\n");
    }

    public static void e(@Nullable String tag, @Nullable String msg){
        if (tag == null || tag.isEmpty()) tag = " ";
        if (msg == null || msg.isEmpty()) msg = " ";
        init();
        write_log(tag, msg);
        e.log(tag, msg);
    }
    public static void w(@Nullable String tag, @Nullable String msg){
        if (tag == null || tag.isEmpty()) tag = " ";
        if (msg == null || msg.isEmpty()) msg = " ";
        init();
        write_log(tag, msg);
        w.log(tag, msg);
    }
    public static void i(@Nullable String tag, @Nullable String msg){
        if (tag == null || tag.isEmpty()) tag = " ";
        if (msg == null || msg.isEmpty()) msg = " ";
        init();
        write_log(tag, msg);
        i.log(tag, msg);
    }
}
