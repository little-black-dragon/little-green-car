package com.example.xiaolvche;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SetupActivity extends AppCompatActivity {

    private Button change_password; //修改密码
    private Button setup_return;    //返回
    private Button clean_cache;     //清除缓存
    private Button check_update;    //检查更新
    private Button about_us;        //关于我们
    private Button customer_service;//平台客服
    private Button legal_provisions;//法律条款与隐私条款
    private Button log_out;         //退出登录



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_center_setup);


        //修改密码跳转
        change_password = findViewById(R.id.mc_change_the_password_button);
        change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.d("==========", "onClick: click");
                Intent intent=new Intent(SetupActivity.this,Setup_change_passwordActivity.class);
                startActivity(intent);
            }
        });

        //返回界面跳转
        setup_return = findViewById(R.id.mc_return);
        setup_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.d("==========", "onClick: click");
                Intent intent=new Intent(SetupActivity.this,activity_personal_center.class);
                startActivity(intent);
            }
        });

        //清理缓存
        clean_cache = findViewById(R.id.mc_clean_the_cache_button);
        clean_cache.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                ProgressDialog progressDialog = new ProgressDialog(SetupActivity.this);
                progressDialog.setCancelable(false);
                progressDialog.setTitle("请等待");
                progressDialog.setMessage("清理中...");
                progressDialog.show();
                Thread thread = new Thread(){
                    public void run(){
                        try {
                            sleep(1000);
                        }catch(InterruptedException e){
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();

                    }
                };
                thread.start();
                Toast.makeText(SetupActivity.this, "清理完成",Toast.LENGTH_SHORT).show();
            }
        });

        //检查更新
        check_update = findViewById(R.id.mc_check_the_update_button);
        check_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressDialog progressDialog = new ProgressDialog(SetupActivity.this);
                progressDialog.setCancelable(false);
                progressDialog.setTitle("请等待");
                progressDialog.setMessage("检查更新中...");
                progressDialog.show();
                Thread thread = new Thread(){
                    public void run(){
                        try {
                            sleep(1000);
                        }catch(InterruptedException e){
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();

                    }
                };
                thread.start();
                Toast.makeText(SetupActivity.this, "当前没有新版本",Toast.LENGTH_SHORT).show();
            }
        });

        //平台客服
        customer_service = findViewById(R.id.mc_customer_service_button);
        customer_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNormalDialog();
            }
        });

        //关于我们
        about_us = findViewById(R.id.mc_about_us_button);
        about_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.d("==========", "onClick: click");
                Intent intent=new Intent(SetupActivity.this,Setup_about_usActivity.class);
                startActivity(intent);
            }
        });

        //法律条款
        legal_provisions = findViewById(R.id.mc_legal_provisions_button);
        legal_provisions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.d("==========", "onClick: click");
                Intent intent=new Intent(SetupActivity.this,Setup_legal_provisions.class);
                startActivity(intent);
            }
        });

        //退出登录
        log_out = findViewById(R.id.mc_log_out_button);
        log_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.d("==========", "onClick: click");
                Intent intent=new Intent(SetupActivity.this,activity_personal_center.class);
                startActivity(intent);
            }
        });
    }
    private void showNormalDialog(){
        /* @setIcon 设置对话框图标
         * @setTitle 设置对话框标题
         * @setMessage 设置对话框消息提示
         * setXXX方法返回Dialog对象，因此可以链式设置属性
         */
        final AlertDialog.Builder normalDialog =
                new AlertDialog.Builder(SetupActivity.this);

        normalDialog.setTitle("客服电话");
        normalDialog.setMessage("拨号：16607919672");
        normalDialog.setPositiveButton("确定",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //...To-do
                    }
                });
        normalDialog.setNegativeButton("关闭",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //...To-do
                    }
                });
        // 显示
        normalDialog.show();
    }
}
