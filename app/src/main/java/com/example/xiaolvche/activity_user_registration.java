package com.example.xiaolvche;

import android.app.Activity;
import android.content.Intent;
import android.media.browse.MediaBrowser;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.xiaolvche.http.HttpConnectionAndCode;
import com.example.xiaolvche.utils.HttpMethod;
import com.example.xiaolvche.utils.Methods;

import org.json.JSONObject;

public class activity_user_registration extends AppCompatActivity {
    Button queding;
    Button quxiao;

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_registration);
        queding = findViewById(R.id.register_btn_sure);
        quxiao = findViewById(R.id.register_btn_cancel);

        EditText editText1 = (EditText) findViewById(R.id.resetpwd_edit_name);
        EditText editText2 = (EditText) findViewById(R.id.resetpwd_edit_pwd_old);
        EditText editText3 = (EditText) findViewById(R.id.resetpwd_edit_pwd_new);
        EditText editText4 = (EditText) findViewById(R.id.phone);
        queding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nike_name = editText1.getText().toString();
                String password = editText3.getText().toString();//默认两次输入密码一样，如需优化可在此添加判断
                String phone = editText4.getText().toString();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        HttpConnectionAndCode res = HttpMethod.UserRegister(nike_name, phone, password);
                        //HttpConnectionAndCode res = HttpMethod.UserRegister(nike_name, phone, password);//调用HttpMethod里的register方法
                        if (res.code == 0) {
                            runOnUiThread(() -> {
                                String nike_name = "";
                                int rcode = -1;
                                String msg = "";
                                try {
                                    JSONObject resj = new JSONObject(res.comment);
                                    rcode = resj.getInt("code");
                                    msg = resj.getString("msg");
                                    JSONObject data = resj.getJSONObject("data");
                                    nike_name = data.getString("nike_name");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                StringBuilder sb = new StringBuilder();
                                if (rcode != 100) {
                                    sb.append("注册失败！").append(Methods.getEmptyStringFromNull(msg));
                                } else {
                                    sb.append("注册成功。").append("昵称：").append(Methods.getEmptyStringFromNull(nike_name));
                                }
                                Toast.makeText(activity_user_registration.this, sb.toString(), Toast.LENGTH_LONG).show();
                            });
                        } else {
                            runOnUiThread(() -> {
                                Toast.makeText(activity_user_registration.this, "注册不成功", Toast.LENGTH_LONG).show();
                            });
                        }
                        Log.d("==========", "onClick: click");
                        startActivity(new Intent(activity_user_registration.this, activity_login_interface2.class));
                    }
                }).start();

            }
        });

        quxiao.setOnClickListener(new View.OnClickListener() {//取消按钮（返回登录界面2）
            @Override
            public void onClick(View v) {
                Log.d("==========", "onClick: click");
                Intent intent = new Intent();
                //intent.setClass(MainActivity.this,activity_fee_scheduling.class);
//                intent.setClass(activity_user_registration.this,Login_activity.class);
                intent.setClass(activity_user_registration.this, activity_login_interface2.class);
                startActivity(intent);
            }
        });

    }
}