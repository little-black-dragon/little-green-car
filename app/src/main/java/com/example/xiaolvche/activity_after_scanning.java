package com.example.xiaolvche;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.MyLocationStyle;
import com.example.xiaolvche.gson.lv_login;
import com.example.xiaolvche.gson.lv_login_s;
import com.example.xiaolvche.gson.lv_scanning;
import com.example.xiaolvche.gson.lv_scanning_s;
import com.example.xiaolvche.http.HttpConnectionAndCode;
import com.example.xiaolvche.utils.HttpMethod;
import com.google.gson.Gson;
import com.orhanobut.logger.Logger;

public class activity_after_scanning extends AppCompatActivity {
    private boolean scanning_success;
    private boolean isFirstLocation;
    private AMap aMap;
    Button use,cancel;

    //声明AMapLocationClient类对象
    public AMapLocationClient mLocationClient = null;
    //声明定位回调监听器
    public AMapLocationListener mLocationListener = new AMapLocationListener() {
        @Override
        public void onLocationChanged(AMapLocation aMapLocation) {
            Logger.d(aMapLocation);
            if (aMapLocation.getErrorCode() == 0 && !isFirstLocation) {
                isFirstLocation = true;
                LatLng latLng = new LatLng(aMapLocation.getLatitude(), aMapLocation.getLongitude());
                CameraPosition position = CameraPosition.fromLatLngZoom(latLng, 16f);

                aMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
            }
        }
    };

    //初始化定位
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_scanning);

        use=findViewById(R.id.Button2);
        cancel=findViewById(R.id.Button1);


        TextView tx_jindu = (TextView) findViewById(R.id.jindu);
        ProgressBar process = (ProgressBar) findViewById(R.id.progressBar);


        new Thread(new Runnable() {
            @Override
            public void run() {
                HttpConnectionAndCode res = HttpMethod.Scanning();
                if(res.code==0){

                    String expra = activity_rent_main.content;
                    Log.e("get content","expra");
                    lv_scanning_s ls = new Gson().fromJson(res.comment, lv_scanning_s.class);
                    if(ls.getCode() == 100){
                        scanning_success = true;

                        lv_scanning ll = ls.getData();
                        int remain_power = ll.getBattery();
                        runOnUiThread(()->{
                            Toast.makeText(activity_after_scanning.this, expra, Toast.LENGTH_LONG).show();
                            tx_jindu.setText(remain_power+"%");
                            process.setProgress(remain_power);//
                        });

//                        Intent intent = new Intent();
//                        runOnUiThread(()->{ Toast.makeText(activity_after_scanning.this,"车辆可用，开始计时收费",Toast.LENGTH_SHORT).show(); });
//                        intent.setClass(activity_after_scanning.this,activity_transport_process.class);
//                        startActivity(intent);
                    }

                    if(ls.getCode() == 102){
//                        runOnUiThread(()->{ Toast.makeText(activity_after_scanning.this,"车辆不可用，请跑步呢",Toast.LENGTH_SHORT).show(); });
                    }

                }

//                        Intent intent = new Intent();
//                        //intent.setClass(MainActivity.this,activity_fee_scheduling.class);
//                        intent.setClass(activity_login_interface2.this,activity_rent_main.class);
//                        startActivity(intent);
            }
        }).start();



        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.d("==========", "onClick: click");
                Intent intent = new Intent();
                //intent.setClass(MainActivity.this,activity_fee_scheduling.class);
              //  intent.setClass(activity_after_scanning.this,activity_rent_main.class);
                startActivity(intent);
            }
        });

        use.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent intent=new Intent();
//                intent.setClass(activity_after_scanning.this,activity_transport_process.class);
//                startActivity(intent);
            }
        });

        MapView mapView = findViewById(R.id.ma_mapview);
        mapView.onCreate(savedInstanceState);
        aMap = mapView.getMap();

        MyLocationStyle myLocationStyle;
        myLocationStyle = new MyLocationStyle();//初始化定位蓝点样式类myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_LOCATION_ROTATE);//连续定位、且将视角移动到地图中心点，定位点依照设备方向旋转，并且会跟随设备移动。（1秒1次定位）如果不设置myLocationType，默认也会执行此种模式。
        myLocationStyle.interval(2000); //设置连续定位模式下的定位间隔，只在连续定位模式下生效，单次定位模式下不会生效。单位为毫秒。
        aMap.setMyLocationStyle(myLocationStyle);//设置定位蓝点的Style
//aMap.getUiSettings().setMyLocationButtonEnabled(true);设置默认定位按钮是否显示，非必需设置。
        aMap.setMyLocationEnabled(true);// 设置为true表示

        mLocationClient = new AMapLocationClient(getApplicationContext());
//设置定位回调监听
        mLocationClient.setLocationListener(mLocationListener);





        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            //申请WRITE_EXTERNAL_STORAGE权限
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION},
                    110);//自定义的code
        } else  {
            mLocationClient.startLocation();
        }

    }
}
