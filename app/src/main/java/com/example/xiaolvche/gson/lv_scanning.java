package com.example.xiaolvche.gson;

public class lv_scanning {
    int id;
    String number;
    String ecu;
    int lock;
    float latitude;
    float longitude;
    String loaction_at;
    int voltage;
    int battery;
    String battery_at;

    public int getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }

    public String getEcu() {
        return ecu;
    }

    public int getLock() {
        return lock;
    }

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public String getLoaction_at() {
        return loaction_at;
    }

    public int getVoltage() {
        return voltage;
    }

    public int getBattery() {
        return battery;
    }

    public String getBattery_at() {
        return battery_at;
    }

}
