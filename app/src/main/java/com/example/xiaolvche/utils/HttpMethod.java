package com.example.xiaolvche.utils;

import com.example.xiaolvche.http.HttpConnectionAndCode;
import com.example.xiaolvche.http.Post;
import com.example.xiaolvche.http.Request;

public class HttpMethod {
    public static String api_url = "http://test.xlceb.com";
    public static String url1 = "http://test.xlceb.com/api/register";
    public static String url2 = "http://test.xlceb.com/api/login";
    public static String url3 = "http://test.xlceb.com/api/ebike?number=077312003";
    public static String url4 = "http://test.xlceb.com/api/order";
    public static String url5 = "http://test.xlceb.com/api/login";
    public static String url6 = "http://test.xlceb.com/api/login";
    public static String agent = "";
    public static String api_cookie = "";
    /**
     * "nickName": "guet2",
     * 	"password": "1234",
     * 	"phoneNum": "1844545233",
     * 	"tenantId": 10
     *
     * */
    public static HttpConnectionAndCode UserRegister(String nike_name, String phone, String password){
        String data = "{\n" +
                "\t\"nike_name\": \"" +nike_name +"\",\n" +
                "\t\"phone\": \"" +phone+ "\",\n" +
                "\t\"password\": \"" +password+ "\"\n" +
                "}";
        HttpConnectionAndCode res = new Request(true).post(
                url1,
                null,
                "",
                "",
                data,
                api_cookie,
                null,
                null,
                null,
                "application/json",
                null,
                Request.CharsetNames.UTF8,
                Request.CharsetNames.UTF8,
                null
        );
        return res;
    }


    public static HttpConnectionAndCode UserLogin(String phone, String password) {
        String data = "{\n" +
                "\t\"phone\": \"" + phone + "\",\n" +
                "\t\"password\": \"" + password + "\"\n" +
                "}";
        HttpConnectionAndCode res = new Request(true).post(
                url2,
                null,
                "",
                "",
                data,
                api_cookie,
                null,
                null,
                null,
                "application/json",
                null,
                Request.CharsetNames.UTF8,
                Request.CharsetNames.UTF8,
                null
        );
        return res;
    }

    public static HttpConnectionAndCode Scanning() {

        HttpConnectionAndCode res = new Request(true).get(
            url3,
            null,
            "",
            "",
            api_cookie,
            null,
            null,
            null,
            null,
                Request.CharsetNames.UTF8,
            null
        );
        return res;
    }

    public static HttpConnectionAndCode CreateOrder(String number) {
        String data = "{\n" +

                "\t\"number\": \"" + number + "\"\n" +
                "}";
        HttpConnectionAndCode res = new Request(true).post(
                url2,
                null,
                "",
                "",
                data,
                api_cookie,
                null,
                null,
                null,
                "application/json",
                null,
                Request.CharsetNames.UTF8,
                Request.CharsetNames.UTF8,
                null
        );
        return res;
    }

}
