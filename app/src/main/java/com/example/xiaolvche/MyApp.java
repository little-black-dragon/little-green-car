package com.example.xiaolvche;

import android.app.Application;
import android.content.pm.ApplicationInfo;

public class MyApp extends Application {
    final public static int default_connect_timeout = 2_000; // 2s
    final public static int default_read_timeout = 20_000; // 20s
    final public static boolean ip_override = false;
    final public static String guet_v_ip = "";
    final public static String guet_v_domain = "";

    private static MyApp app;

    public static MyApp getCurrentApp(){
        return app;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
    }

    public static boolean isDebug(){
        return ((app.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0);
    }
}
