package JSonClass;

import com.google.gson.annotations.Expose;

public class UserLogin {
    private  String startTime;
    private  String endTime;
    private  int startLat;
    private  int startLng;
    private  int endLat;
    private  int endLng;
    private  int perMin;
    private  int perCost;
    private  String carId;
    @Expose
    private String nickName;//用户id

    @Expose
    private String password;//用户密码
    @Expose
    private int tenantId;//
    @Expose
    private String phoneNum;//电话号码
    @Expose
    private String userName;//用户名字
    @Expose
    private String idCard;//用户名字
    @Expose
    private int status;//状态码
    @Expose
    private int userId;//
    @Expose
    private int id;//

    public UserLogin(String nickName, String password, int tenantId ){//登录
        this.nickName=nickName;
        this.password=password;
        this.tenantId=tenantId;
    }
    public UserLogin(String nickName, String password, int tenantId, String  phoneNum ){//注册
        this.nickName=nickName;
        this.password=password;
        this.tenantId=tenantId;
        this.phoneNum=phoneNum;
    }
    public UserLogin( int tenantId ){//订单 获取全部会员类型 获取全部卡券类型 获取近七天的用户新增数据
        this.tenantId=tenantId;

    }
    public UserLogin(int userId, int tenantId ){//会员信息查询  卡券信息查询//
        this.tenantId=tenantId;
        this.userId=userId;
    }
    public UserLogin(int id, int tenantId,int status ){//用户押金信息查询 计价标准获取
        this.tenantId=tenantId;
        this.id=id;
        this.status=status;
    }
    public UserLogin(int userId,String userName,String idCard,int status,int tenantId ){//身份认证
        this.userId=userId;
        this.userName=userName;
        this.tenantId=tenantId;
        this.idCard=idCard;
        this.status=status;
    }
    public UserLogin(int tenantId,int userId,String carId ,String startTime,String endTime,int startLat,int startLng ,int endLat,int endLng,int perMin,int perCost  ) {//订单生成
        this.userId=userId;
        this.tenantId=tenantId;
        this.carId=carId;
        this.startTime=startTime;
        this.endTime=endTime;
        this.startLat=startLat;
        this.startLng=startLng;
        this.endLat=endLat;
        this.endLng=endLng;
        this.perMin=perMin;
        this.perCost=perCost;



    }
}
