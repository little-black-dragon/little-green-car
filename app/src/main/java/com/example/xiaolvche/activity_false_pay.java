package com.example.xiaolvche;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;


public class activity_false_pay extends AppCompatActivity {
    Button zhifu;
    Button kefu;
    Button back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_personal_center);
        setContentView(R.layout.activity_false_pay);
        back=findViewById(R.id.fanhui);
        zhifu=findViewById(R.id.zhifu);
        kefu=findViewById(R.id.kefu);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("==========", "onClick: click");
                Intent intent=new Intent();
                intent.setClass(activity_false_pay.this,activity_transport_process.class);
                startActivity(intent);
            }
        });
        kefu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("==========", "onClick: click");
                Intent intent=new Intent();
                intent.setClass(activity_false_pay.this,activity_fee_scheduling.class);
                startActivity(intent);
            }
        });
        zhifu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("==========", "onClick: click");
                Intent intent=new Intent();
                intent.setClass(activity_false_pay.this,activity_after_scanning.class);
                startActivity(intent);
            }
        });
    }//
}

