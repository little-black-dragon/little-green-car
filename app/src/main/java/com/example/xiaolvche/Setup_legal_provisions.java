package com.example.xiaolvche;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class Setup_legal_provisions extends AppCompatActivity {

    private Button legal_provisions_return;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_up_legal_provisions);

        legal_provisions_return = findViewById(R.id.mc_return);
        legal_provisions_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.d("==========", "onClick: click");
                Intent intent=new Intent(Setup_legal_provisions.this,SetupActivity.class);
                startActivity(intent);
            }
        });

    }
}
