package com.example.xiaolvche;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.MyLocationStyle;
//import com.example.xiaolvche.zxing.android.CaptureActivity;
import com.example.xiaolvche.zxing.android.CaptureActivity;
import com.orhanobut.logger.Logger;




public class activity_rent_main  extends AppCompatActivity implements View.OnClickListener{
    private boolean isFirstLocation;
    private AMap aMap;

    private static final String DECODED_CONTENT_KEY = "codedContent";
    private static final String DECODED_BITMAP_KEY = "codedBitmap";
    private static final int REQUEST_CODE_SCAN = 0x0000;
//    private TextView tv_scanResult;

    public static String content = null;
    public static Bitmap bitmap = null;

    private Button button,button1;

    //声明AMapLocationClient类对象
    public AMapLocationClient mLocationClient = null;
    //声明定位回调监听器
    public AMapLocationListener mLocationListener = new AMapLocationListener() {
        @Override
        public void onLocationChanged(AMapLocation aMapLocation) {
            Logger.d(aMapLocation);
            if (aMapLocation.getErrorCode() == 0 && !isFirstLocation) {
                isFirstLocation = true;
                LatLng latLng = new LatLng(aMapLocation.getLatitude(), aMapLocation.getLongitude());
                CameraPosition position = CameraPosition.fromLatLngZoom(latLng, 16f);

                aMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
            }
        }
    };


    //初始化定位
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rent_main);

        MapView mapView = findViewById(R.id.ma_mapview);
        mapView.onCreate(savedInstanceState);
        aMap = mapView.getMap();
        MyLocationStyle myLocationStyle;

        button=(Button)findViewById(R.id.Button2);
//        button.setOnClickListener(this);
        button1=(Button)findViewById(R.id.ButtonAvatar);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                intent.setClass(activity_rent_main.this,activity_personal_center.class);
                startActivity(intent);
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //动态权限申请
                if (ContextCompat.checkSelfPermission(activity_rent_main.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity_rent_main.this, new String[]{Manifest.permission.CAMERA}, 1);
                } else {
                    goScan();
                }
//                break;
//                default:
//                break;
            }
        });




        //初始化定位蓝点样式类myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_LOCATION_ROTATE);
        //连续定位、且将视角移动到地图中心点，定位点依照设备方向旋转，并且会跟随设备移动。（1秒1次定位）如果不设置myLocationType，默认也会执行此种模式。
        myLocationStyle = new MyLocationStyle();
        //设置连续定位模式下的定位间隔，只在连续定位模式下生效，单次定位模式下不会生效。单位为毫秒。
        myLocationStyle.interval(2000);
        //设置定位蓝点的Style
        aMap.setMyLocationStyle(myLocationStyle);
        //aMap.getUiSettings().setMyLocationButtonEnabled(true);设置默认定位按钮是否显示，非必需设置。
        aMap.setMyLocationEnabled(true);//设置为true表示
        mLocationClient = new AMapLocationClient(getApplicationContext());
        //设置定位回调监听
        mLocationClient.setLocationListener(mLocationListener);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            //申请WRITE_EXTERNAL_STORAGE权限
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION},
                    110);//自定义的code
        } else  {
            mLocationClient.startLocation();
        }

    }


    @Override
    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.Button2:
//                //动态权限申请
//                if (ContextCompat.checkSelfPermission(activity_rent_main.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//                    ActivityCompat.requestPermissions(activity_rent_main.this, new String[]{Manifest.permission.CAMERA}, 1);
//                } else {
//                    goScan();
//                }
//                break;
//            default:
//                break;
//            case R.id.ButtonAvatar:
//                Intent intent=new Intent();
//                intent.setClass(activity_rent_main.this,activity_personal_center.class);
//                startActivity(intent);
//                finish();
//        }

    }
    /**
     * 跳转到扫码界面扫码
     */

    private void goScan(){
        Intent intent = new Intent(activity_rent_main.this, CaptureActivity.class);
        startActivityForResult(intent, REQUEST_CODE_SCAN);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    goScan();
                } else {
                    Toast.makeText(this, "你拒绝了权限申请，可能无法打开相机扫码哟！", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 扫描二维码/条码回传
        if (requestCode == REQUEST_CODE_SCAN && resultCode == RESULT_OK) {
            if (data != null) {
                //返回的文本内容
                content = data.getStringExtra(DECODED_CONTENT_KEY);
                Log.e("set content","content");
                //返回的BitMap图像
                bitmap = data.getParcelableExtra(DECODED_BITMAP_KEY);

//                tv_scanResult.setText("你扫描到的内容是：" + content);
            }
        }
    }

}
