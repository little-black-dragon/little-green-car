package com.example.xiaolvche;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class activity_personal_center extends AppCompatActivity {
    //按钮声明
    Button useguide;
    Button message;
    Button shenfen;
    Button kefu;
    Button setup;
    Button personal_return;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_center);
        //按钮初始化
        useguide = findViewById(R.id.mc_use_guide_value);
        message = findViewById(R.id.mc_message_value);
        shenfen = findViewById(R.id.mc_identity_value);
        kefu = findViewById(R.id.mc_customerservice_value);
        setup = findViewById(R.id.mc_Setup_value);
        personal_return = findViewById(R.id.mc_return_button);

        //使用指南跳转
        useguide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.d("==========", "onClick: click");
                Intent intent = new Intent();
                intent.setClass(activity_personal_center.this, activity_personal_center_useguide.class);
                startActivity(intent);
            }
        });

        //返回跳转
        personal_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.d("==========", "onClick: click");
                Intent intent = new Intent();
                intent.setClass(activity_personal_center.this, activity_rent_main.class);
                startActivity(intent);
                finish();
            }
        });

        //消息跳转
        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.d("==========", "onClick: click");
                Intent intent = new Intent();
                intent.setClass(activity_personal_center.this, activity_personal_center_message.class);
                startActivity(intent);
            }
        });

        //身份认证跳转
        shenfen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.d("==========", "onClick: click");
                Intent intent = new Intent();
                intent.setClass(activity_personal_center.this, activity_personal_center_authentication.class);
                startActivity(intent);
            }
        });

        //客服弹窗
        kefu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNormalDialog();
            }
        });

        //设置跳转
        setup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.d("==========", "onClick: click");
                Intent intent = new Intent();
                intent.setClass(activity_personal_center.this, SetupActivity.class);
                startActivity(intent);
            }
        });


    }

    private void showNormalDialog(){
        /* @setIcon 设置对话框图标
         * @setTitle 设置对话框标题
         * @setMessage 设置对话框消息提示
         * setXXX方法返回Dialog对象，因此可以链式设置属性
         */
        final AlertDialog.Builder normalDialog =
                new AlertDialog.Builder(activity_personal_center.this);

        normalDialog.setTitle("客服电话");
        normalDialog.setMessage("拨号：16607919672");
        normalDialog.setPositiveButton("确定",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //...To-do
                    }
                });
        normalDialog.setNegativeButton("关闭",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //...To-do
                    }
                });
        // 显示
        normalDialog.show();
    }


}