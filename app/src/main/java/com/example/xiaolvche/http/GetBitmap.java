package com.example.xiaolvche.http;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.xiaolvche.MyApp;
import com.example.xiaolvche.utils.Methods;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.zip.GZIPInputStream;


public class GetBitmap {
    /**
     * @return - 0 GET success
     * - -1 cannot open url
     * - -5 cannot get response
     * - -6 response check fail
     * - -7 302
     * @non-ui
     * @clear
     */
    static HttpConnectionAndCode get(@NonNull final String u,
                                            @Nullable final String[] parms,
                                            @NonNull final String user_agent,
                                            @NonNull String referer,
                                            @Nullable final String cookie,
                                            @Nullable final String cookie_delimiter,
                                            @Nullable Boolean quick_test
                                            ) {
        URL url = null;
        HttpURLConnection cnt = null;
        String response = null;
        Bitmap bmp = null;
        int resp_code = 0;
        try {
            StringBuilder u_bulider = new StringBuilder();
            u_bulider.append(u);
            if (parms != null && parms.length > 0) {
                u_bulider.append("?").append(TextUtils.join("&", parms));
            }
            String url_str = UrlProcess.process(u_bulider.toString());
//            url_str = Proxy.replace(url_str);
//            referer = Proxy.replace(referer);
            url = new URL(url_str);
            cnt = Proxy.setProxy(url_str, url);

//            cnt = (HttpURLConnection) url.openConnection();

            cnt.setDoInput(true);
            cnt.setRequestProperty("User-Agent", user_agent);
            cnt.setRequestProperty("Referer", UrlProcess.process(referer));
            if (cookie != null) {
                cnt.setRequestProperty("Cookie", cookie);
            }
            cnt.setRequestMethod("GET");
            cnt.setInstanceFollowRedirects(false);
            if (quick_test == null) quick_test = false;
            cnt.setConnectTimeout(quick_test? 500 : MyApp.default_connect_timeout);
            cnt.setReadTimeout(quick_test? 500 :MyApp.default_read_timeout);
            cnt.connect();
        } catch (Exception e) {
            e.printStackTrace();
            return new HttpConnectionAndCode(-1);
        }
        try {
            resp_code = cnt.getResponseCode();
            if (
                    (
                            resp_code == 301 ||
                                    resp_code == 302 ||
                                    resp_code == 303 ||
                                    resp_code == 307 ||
                                    resp_code == 308
                    )
            ) {
                return new HttpConnectionAndCode(-7).setC(cnt).setResp_code(resp_code);
            }
            response = "";
            List<String> encodings = cnt.getHeaderFields().get("content-encoding");
            if (encodings != null && encodings.contains("gzip")) {
                bmp = BitmapFactory.decodeStream(new GZIPInputStream(cnt.getInputStream()));
            }else {
                bmp = BitmapFactory.decodeStream(cnt.getInputStream());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new HttpConnectionAndCode(-5).setC(cnt);
        }

        //do not disconnect, keep alive
        //if cookie_delimiter != null but no server cookie, set_cookie = ""
        //if no response, response = ""
        return new HttpConnectionAndCode(0).setAll(cnt, resp_code, response, Methods.getEmptyStringFromNull(Request.get_response_cookie(cnt, cookie_delimiter)), bmp);
    }
}
