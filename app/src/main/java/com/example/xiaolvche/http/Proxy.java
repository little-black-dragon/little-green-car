package com.example.xiaolvche.http;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URL;

public class Proxy {
    public static HttpURLConnection setProxy(String s, URL url) throws IOException {
        return (HttpURLConnection) url.openConnection();
    }
}
