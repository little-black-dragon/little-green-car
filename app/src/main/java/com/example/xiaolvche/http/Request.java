package com.example.xiaolvche.http;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.net.CookieManager;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Request {

    public static class CharsetNames{
        public static String UTF8 = "UTF-8";
        public static String GBK = "GBK";
    }
    public static class ContentType{
        public static String form_url_encoded = "application/x-www-form-urlencoded";
        public static String form_url_encoded_utf8 = "application/x-www-form-urlencoded;charset=UTF-8";
    }

    public static final String UA = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36";
    public static final String COOKIE_DELIMITER = ";";
    public static final boolean HTTP = true;
    public static final boolean HTTPS = false;

    public static final String lan_head = "http://172.16.13.22";
    public static final String wan_head = "https://v.guet.edu.cn/http/77726476706e69737468656265737421a1a013d2766626012d46dbfe";

    public static final String lan_lib_head = "http://202.193.70.139";
    public static final String wan_lib_head = "https://v.guet.edu.cn/http/77726476706e69737468656265737421a2a713d276693b1e2958c7fdcb0c";

    public static final String lan_cxcy_head = "http://172.16.64.212";
    public static final String wan_cxcy_head = "https://v.guet.edu.cn/http/77726476706e69737468656265737421a1a013d2766626062a46dbfdca";

    public static final String lan_lib_position_head = "http://202.193.70.137";
    public static final String wan_lib_position_head = "https://v.guet.edu.cn/http/77726476706e69737468656265737421a2a713d276693b1e2958c7fdcb02";

    public static final String lan_icampus_head = "http://icampus.guet.edu.cn";
    public static final String wan_icampus_head = "https://v.guet.edu.cn/http/77726476706e69737468656265737421f9f4409137257b1e791d8cb8d6502720b11d95";

    public static final String lan_both_https_cas_head = "https://cas.guet.edu.cn";
    public static final String wan_both_https_cas_head = "https://v.guet.edu.cn/https/77726476706e69737468656265737421f3f652d220256d44300d8db9d6562d";

    private static String lwan(@NonNull String url, boolean isLan){
        return isLan?
                url.replaceFirst(wan_head, lan_head).replaceFirst(wan_lib_head, lan_lib_head).replaceFirst(wan_lib_position_head, lan_lib_position_head).replaceFirst(wan_cxcy_head, lan_cxcy_head)
                .replaceFirst(wan_icampus_head, lan_icampus_head)
                :
                url.replaceFirst(lan_head, wan_head).replaceFirst(lan_lib_head, wan_lib_head).replaceFirst(lan_lib_position_head, wan_lib_position_head).replaceFirst(lan_cxcy_head, wan_cxcy_head)
                .replaceFirst(lan_icampus_head, wan_icampus_head)
                ;
    }

    public static String get_lwan(@NonNull String url, boolean isLan){
        return lwan(url, isLan);
    }

    public static String get_lwan_for_both_https(@NonNull String url, boolean isLan){
        return isLan?
                url.replaceFirst(wan_both_https_cas_head, lan_both_https_cas_head)
                :
                url.replaceFirst(lan_both_https_cas_head, wan_both_https_cas_head)
                ;
    }



    private final boolean isLAN;

    public Request(boolean isLANOrIsHttp) {
        this.isLAN = isLANOrIsHttp;
    }

    private HttpConnectionAndCode get_in(
            @NonNull String u,
            @Nullable final String[] parms,
            @NonNull final String user_agent,
            @NonNull String referer,
            @Nullable final String cookie,
            @Nullable final String cookie_delimiter,
            @Nullable final String success_resp_text,
            @Nullable final String[] accept_encodings,
            @Nullable final Map.Entry<String, String>[] headers,
            @Nullable final Boolean redirect,
            @Nullable String read_charset_name,
            @Nullable Boolean quick_test,
            @Nullable Integer connect_timeout,
            @Nullable Integer read_timeout
    ){
        return isLAN?
                Get.get(
                        u, parms, user_agent, referer, cookie, cookie_delimiter, success_resp_text, accept_encodings, headers, redirect, read_charset_name, quick_test, connect_timeout, read_timeout
                ):
                Get_https.get(
                        u, parms, user_agent, referer, cookie, cookie_delimiter, success_resp_text, accept_encodings, headers, redirect, read_charset_name, quick_test, connect_timeout, read_timeout
                );
    }

    public HttpConnectionAndCode get(
            @NonNull String u,
            @Nullable final String[] parms,
            @NonNull final String user_agent,
            @NonNull String referer,
            @Nullable final String cookie,
            @Nullable final String cookie_delimiter,
            @Nullable final String success_resp_text,
            @Nullable final String[] accept_encodings,
            @Nullable final Boolean redirect,
            @Nullable String read_charset_name,
            @Nullable Boolean quick_test
    ) {
        return get_in(
                u, parms, user_agent, referer, cookie, cookie_delimiter, success_resp_text, accept_encodings, null, redirect, read_charset_name, quick_test, null, null
        );
    }

    public HttpConnectionAndCode get(
            @NonNull String u,
            @Nullable final String[] parms,
            @NonNull final String user_agent,
            @NonNull String referer,
            @Nullable final String cookie,
            @Nullable final String cookie_delimiter,
            @Nullable final String success_resp_text,
            @Nullable final String[] accept_encodings,
            @Nullable final Map.Entry<String, String>[] headers,
            @Nullable final Boolean redirect,
            @Nullable String read_charset_name,
            @Nullable Boolean quick_test
    ) {
        return get_in(
                u, parms, user_agent, referer, cookie, cookie_delimiter, success_resp_text, accept_encodings, headers, redirect, read_charset_name, quick_test, null, null
        );
    }

    public HttpConnectionAndCode get(
            @NonNull String u,
            @Nullable final String[] parms,
            @NonNull final String user_agent,
            @NonNull String referer,
            @Nullable final String cookie,
            @Nullable final String cookie_delimiter,
            @Nullable final String success_resp_text,
            @Nullable final String[] accept_encodings,
            @Nullable final Boolean redirect,
            @Nullable String read_charset_name,
            @Nullable Boolean quick_test,
            int connect_timeout,
            int read_timeout
    ) {
        return get_in(
                u, parms, user_agent, referer, cookie, cookie_delimiter, success_resp_text, accept_encodings, null, redirect, read_charset_name, quick_test, connect_timeout, read_timeout
        );
    }

    public HttpConnectionAndCode get(
            @NonNull String u,
            @Nullable final String[] parms,
            @NonNull final String user_agent,
            @NonNull String referer,
            @Nullable final String cookie,
            @Nullable final String cookie_delimiter,
            @Nullable final String success_resp_text,
            @Nullable final String[] accept_encodings,
            @Nullable final Map.Entry<String, String>[] headers,
            @Nullable final Boolean redirect,
            @Nullable String read_charset_name,
            @Nullable Boolean quick_test,
            int connect_timeout,
            int read_timeout
    ) {
        return get_in(
                u, parms, user_agent, referer, cookie, cookie_delimiter, success_resp_text, accept_encodings, headers, redirect, read_charset_name, quick_test, connect_timeout, read_timeout
        );
    }

    private HttpConnectionAndCode post_in(
            @NonNull String u,
            @Nullable final String[] parms,
            @NonNull final String user_agent,
            @NonNull String referer,
            @Nullable final String data,
            @Nullable final String cookie,
            @Nullable final String cookie_delimiter,
            @Nullable final String success_resp_text,
            @Nullable final String[] accept_encodings,
            @Nullable final Map.Entry<String, String>[] headers,
            @Nullable final String content_type,
            @Nullable final Boolean redirect,
            @Nullable String write_charset_name,
            @Nullable String read_charset_name,
            @Nullable Boolean quick_test,
            @Nullable Integer connect_timeout,
            @Nullable Integer read_timeout
    ){
        return isLAN?
                Post.post(
                        u, parms, user_agent, referer, data, cookie, cookie_delimiter, success_resp_text, accept_encodings, headers, content_type, redirect, read_charset_name, write_charset_name, quick_test, connect_timeout, read_timeout
                ):
                Post_https.post(
                        u, parms, user_agent, referer, data, cookie, cookie_delimiter, success_resp_text, accept_encodings, headers, content_type, redirect, read_charset_name, write_charset_name, quick_test, connect_timeout, read_timeout
                );
    }

    public HttpConnectionAndCode post(
            @NonNull String u,
            @Nullable final String[] parms,
            @NonNull final String user_agent,
            @NonNull String referer,
            @Nullable final String data,
            @Nullable final String cookie,
            @Nullable final String cookie_delimiter,
            @Nullable final String success_resp_text,
            @Nullable final String[] accept_encodings,
            @Nullable final String content_type,
            @Nullable final Boolean redirect,
            @Nullable String write_charset_name,
            @Nullable String read_charset_name,
            @Nullable Boolean quick_test
    ) {
        return post_in(
                u, parms, user_agent, referer, data, cookie, cookie_delimiter, success_resp_text, accept_encodings, null, content_type, redirect, read_charset_name, write_charset_name, quick_test, null, null
        );
    }

    public HttpConnectionAndCode post(
            @NonNull String u,
            @Nullable final String[] parms,
            @NonNull final String user_agent,
            @NonNull String referer,
            @Nullable final String data,
            @Nullable final String cookie,
            @Nullable final String cookie_delimiter,
            @Nullable final String success_resp_text,
            @Nullable final String[] accept_encodings,
            @Nullable final Map.Entry<String, String>[] headers,
            @Nullable final String content_type,
            @Nullable final Boolean redirect,
            @Nullable String write_charset_name,
            @Nullable String read_charset_name,
            @Nullable Boolean quick_test
    ) {
        return post_in(
                u, parms, user_agent, referer, data, cookie, cookie_delimiter, success_resp_text, accept_encodings, headers, content_type, redirect, read_charset_name, write_charset_name, quick_test, null, null
        );
    }

    public HttpConnectionAndCode post(
            @NonNull String u,
            @Nullable final String[] parms,
            @NonNull final String user_agent,
            @NonNull String referer,
            @Nullable final String data,
            @Nullable final String cookie,
            @Nullable final String cookie_delimiter,
            @Nullable final String success_resp_text,
            @Nullable final String[] accept_encodings,
            @Nullable final String content_type,
            @Nullable final Boolean redirect,
            @Nullable String write_charset_name,
            @Nullable String read_charset_name,
            @Nullable Boolean quick_test,
            int connect_timeout,
            int read_timeout
    ) {
        return post_in(
                u, parms, user_agent, referer, data, cookie, cookie_delimiter, success_resp_text, accept_encodings, null, content_type, redirect, read_charset_name, write_charset_name, quick_test, connect_timeout, read_timeout
        );
    }

    public HttpConnectionAndCode post(
            @NonNull String u,
            @Nullable final String[] parms,
            @NonNull final String user_agent,
            @NonNull String referer,
            @Nullable final String data,
            @Nullable final String cookie,
            @Nullable final String cookie_delimiter,
            @Nullable final String success_resp_text,
            @Nullable final String[] accept_encodings,
            @Nullable final Map.Entry<String, String>[] headers,
            @Nullable final String content_type,
            @Nullable final Boolean redirect,
            @Nullable String write_charset_name,
            @Nullable String read_charset_name,
            @Nullable Boolean quick_test,
            int connect_timeout,
            int read_timeout
    ) {
        return post_in(
                u, parms, user_agent, referer, data, cookie, cookie_delimiter, success_resp_text, accept_encodings, headers, content_type, redirect, read_charset_name, write_charset_name, quick_test, connect_timeout, read_timeout
        );
    }

    static String get_response_cookie(@NonNull HttpURLConnection cnt, @Nullable String cookie_delimiter){
        //get cookie from server
        String set_cookie = null;
        if (cookie_delimiter != null) {
            CookieManager cookieman = new CookieManager();
            StringBuilder cookie_builder = new StringBuilder();
            //getHeaderFields() returns the header fields of response
            List<String> cookies = cnt.getHeaderFields().get("Set-Cookie");
            if (cookies != null) {
                for (String cookie_resp : cookies) {
                    cookieman.getCookieStore().add(null, HttpCookie.parse(cookie_resp).get(0));
                }
            }
            if (cookieman.getCookieStore().getCookies().size() > 0) {
                List<HttpCookie> cookieList = cookieman.getCookieStore().getCookies();
                List<String> cookieStringList = new LinkedList<>();
                for (HttpCookie httpCookie : cookieList) {
                    String str = httpCookie.getName() + "=" + httpCookie.getValue();
                    cookieStringList.add(str);
                }
                String cookie_join = TextUtils.join(cookie_delimiter, cookieStringList);
                cookie_builder.append(cookie_join);
            }
            set_cookie = cookie_builder.toString();
        }
        return set_cookie;
    }

    public HttpConnectionAndCode bitmap(
            @NonNull String u,
            @Nullable final String[] parms,
            @NonNull final String user_agent,
            @NonNull String referer,
            @Nullable final String cookie,
            @Nullable final String cookie_delimiter,
            @Nullable Boolean quick_test
    ){
        u = lwan(u, isLAN);
        referer = lwan(referer, isLAN);
        return isLAN?
                GetBitmap.get(
                        u, parms, user_agent, referer, cookie, cookie_delimiter, quick_test
                ):
                GetBitmap_https.get(
                        u, parms, user_agent, referer, cookie, cookie_delimiter, quick_test
                );
    }
}
