package com.example.xiaolvche;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.xiaolvche.gson.lv_login;
import com.example.xiaolvche.gson.lv_login_s;
import com.example.xiaolvche.http.HttpConnectionAndCode;
import com.example.xiaolvche.utils.HttpMethod;
import com.google.gson.Gson;


public class activity_login_interface2 extends AppCompatActivity {
    Button denglu1;
    Button fanhui1;
    Button tuichu;
    //Button yanzhenma1;

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_interface2);
        denglu1 = findViewById(R.id.denglul);
        //yanzhenma1 = findViewById(R.id.yanzhenma1);
        String inputText;

        EditText editText1=(EditText)findViewById(R.id.editTextTextPersonName);
        EditText editText2=(EditText)findViewById(R.id.editTextTextPassword);

        denglu1.setOnClickListener(new View.OnClickListener() {//登陆按钮
            @Override
            public void onClick(View v) {
                String phone = editText1.getText().toString();
                String password = editText2.getText().toString();
                if (editText1.getText().toString().length() <= 0) {
                    runOnUiThread(()->{ Toast.makeText(activity_login_interface2.this,"手机号不能为空",Toast.LENGTH_SHORT).show(); });
                    return;
                }
                if (editText2.getText().toString().length() <= 0) {
                    runOnUiThread(()->{ Toast.makeText(activity_login_interface2.this,"密码不能为空",Toast.LENGTH_SHORT).show(); });
                    return;
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        HttpConnectionAndCode res = HttpMethod.UserLogin(phone,password);
                        //HttpConnectionAndCode res = HttpMethod.UserLogin(phone,password);
                        if(res.code==0){

                            lv_login_s ls = new Gson().fromJson(res.comment, lv_login_s.class);
                            if(ls.getCode() == 100){
                                Intent intent = new Intent();
                                runOnUiThread(()->{ Toast.makeText(activity_login_interface2.this,"登陆成功",Toast.LENGTH_SHORT).show(); });
                                intent.setClass(activity_login_interface2.this,activity_rent_main.class);
                                startActivity(intent);
                                return;
                            }

                            if(ls.getCode() == 102){
                                runOnUiThread(()->{ Toast.makeText(activity_login_interface2.this,"未找到用户，请先注册吧",Toast.LENGTH_SHORT).show(); });
                            }
                            lv_login ll = ls.getData();


                        }

//                        Intent intent = new Intent();
//                        //intent.setClass(MainActivity.this,activity_fee_scheduling.class);
//                        intent.setClass(activity_login_interface2.this,activity_rent_main.class);
//                        startActivity(intent);
                    }
                }).start();
            }
        });

        /*yanzhenma1.setOnClickListener(new View.OnClickListener() {//验证码按钮
            @Override
            public void onClick(View v) {

                Intent intent = new Intent();
                //intent.setClass(MainActivity.this,activity_fee_scheduling.class);
                intent.setClass(activity_login_interface2.this,Login_activity.class);
                startActivity(intent);
            }
        });*/
    }//
}